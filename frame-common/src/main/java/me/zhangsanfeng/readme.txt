//模块提供的功能
1.entity、dao、service、controller基类
2.所有工具类-util
3.自定义注解-annotation
4.全局异常处理-exception
5.自定义拦截器-intercepter
6.自定义过滤器-filter