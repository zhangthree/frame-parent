package me.zhangsanfeng.frame.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    /*http状态码*/
    private Integer httpCode;

    /*提示信息 */
    private String message;

    /*具体内容*/
    private  T data;
}
