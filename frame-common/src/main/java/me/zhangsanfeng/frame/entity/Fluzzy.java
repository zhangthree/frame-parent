package me.zhangsanfeng.frame.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName:Fluzzy
 * @Description:TODO
 * @Author:ZhangYao
 * @Date:2018/10/22 02:17
 * @Version:1.0
 */
@Data
public class Fluzzy implements Serializable {
    private static final long serialVersionUID=1L;
    //private String and;
    private String or;
    private String in;
}
