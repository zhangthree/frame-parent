package me.zhangsanfeng.frame.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName:Fields
 * @Description:TODO
 * @Author:ZhangYao
 * @Date:2018/10/22 02:11
 * @Version:1.0
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Fields implements Serializable {
    private static final long serialVersionUID=1L;
    private String include;
    private String except;
}
