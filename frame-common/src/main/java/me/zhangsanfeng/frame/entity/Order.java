package me.zhangsanfeng.frame.entity;

import lombok.Data;

/**
 * @ClassName:Order
 * @Description:TODO
 * @Author:ZhangYao
 * @Date:2018/10/24 15:16
 * @Version:1.0
 */
@Data
public class Order {
    private static final long serialVersionUID=1L;
    private String asc;
    private String desc;
}
