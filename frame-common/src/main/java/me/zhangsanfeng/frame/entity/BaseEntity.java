package me.zhangsanfeng.frame.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField(exist = false)
    private Timestamp insertTime;
    @TableField(exist = false)
    private Integer insertBy;
    @TableField(exist = false)
    private Timestamp updateTime;
    @TableField(exist = false)
    private Integer updateBy;
    @TableField(exist = false)
    private Integer pageSize;
    @TableField(exist = false)
    private Integer pageNum;
    /*查询操作*/
    /*返回字段*/
    @TableField(exist = false)
    private Fields fields = new Fields();
    /*条件关键字*/
    @TableField(exist = false)
    private Fluzzy fluzzy = new Fluzzy();
    /*排序*/
    @TableField(exist = false)
    private Order order = new Order();
    /*拼接where条件*/
    @TableField(exist = false)
    private String whereClause;

}
