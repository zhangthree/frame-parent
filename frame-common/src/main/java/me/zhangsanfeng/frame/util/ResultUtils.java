package me.zhangsanfeng.frame.util;

import me.zhangsanfeng.frame.entity.Result;

public class ResultUtils {

    public static Result success(Object object){
        return new Result(200,"success",object);
    }

    public static Result success(String message,Object object){
        return new Result(200,message,object);
    }

    public static Result error(String message,Object object){
        return new Result(500,message,object);
    }

}
