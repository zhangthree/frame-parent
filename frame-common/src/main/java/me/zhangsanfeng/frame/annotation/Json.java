package me.zhangsanfeng.frame.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName:Json
 * @Description:自定义json返回值过滤
 * @Author:ZhangYao
 * @Date:2018/10/26 22:19
 * @Version:1.0
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Json {

    String[] include() default {};

    String[] filter() default {};

}
